% 2in PVC pipe loss data from http://www.engineeringtoolbox.com/pvc-pipes-friction-loss-d_802.html
gpm2  = [5,      7,  10,  15,  20,  25,  30,  35,  40,  45,  50,  60];
loss2 = [0.07, 0.1, 0.2, 0.5, 0.8, 1.2, 1.6, 2.2, 2.8, 3.4, 4.2, 5.8];
pvc2LossFit = polyfit(gpm2,loss2, 2);
pvc2per100 = 776/100;
valve1Cv = 19;
valve2Cv = 30;

coilLengthFt = 84/12; % 7ft.

% Note.  Ignoring 1" and 1-1/4" pipe losses as I assume they will be
% changed when a Chiller is installed

% Load the coil data collected from Super Radiator Coils using iMacro
% coil = loadCoilData('allData.xlsx','allData',2,5009);
coil = loadCoilData('FullHalfQ.xlsx','allData',2,2143);


% Calculate the Sensible Heating Ratio
coil.SHR = coil.SensibleCap./coil.TotalCap;

coil.FinTypeNumber = zeros(size(coil.Idx));
coil.FinTypeNumber(strcmp(coil.FinSurface, 'Flat')) = 5;
coil.FinTypeNumber(strcmp(coil.FinSurface, 'Corrugated')) = 7;

% Calculate total achieved Capacity in Tons
coil.CapacityTons = coil.TotalCap./12000;

% copper tube is about $1/ft
coil.CapitalCost = coil.Rows .* coil.NumberFaceRows .* coilLengthFt;

% based on http://www.engineeringtoolbox.com/fans-efficiency-power-consumption-d_197.html
% Pcfm = 0.1175 qcfm dpin / (�f  �b �m)
% where
% Pcfm = power consumption (W)
% qcfm =  volume flow (cfm)
% dpin = pressure increase (in. WG)
% Typical motor and belt efficiencies:
%  - Motor 1kW - 0.4
%  - Motor 10 kW - 0.87
%  - Motor 100 kW - 0.92
%  - Belt 1 kW - 0.78
%  - Belt 10 kW - 0.88
%  - Belt 100 kW - 0.93
uf = 0.90;
um = 0.87;
ub = 0.88;
ud = 0.94;
coil.FanKw = ((0.1175.*coil.CFM.*(coil.AirFriction+1.75)) ./ (uf*um*ub*ud))./1000;
coil.FanKwPerTon = coil.FanKw./coil.CapacityTons;

% Ph(kW) = 0.7457*q*h / (3960*up) 
% where
% Ph(kW) = shaft power (kW)
% q = flow capacity (gpm)
% h = differential head (ft)
up = 0.6;
coil.pvc2InHead = polyval(pvc2LossFit, coil.GPM)*pvc2per100;
coil.TotalHead = coil.FluidPressureDrop + ... % Pressure Loss due to Coil
                 (coil.GPM./valve1Cv) + ... % Pressure Loss due to Valve 1
                 (coil.GPM./valve2Cv) + ... % Pressure Loss due to Valve 2
                 (coil.pvc2InHead); % Pressure Loss due to PVC 2"
coil.PumpKw = (0.7457.*coil.GPM.*coil.TotalHead) ./ (3960*up);
coil.PumpKwPerTon = coil.PumpKw./coil.CapacityTons;

% calculate Heatpump operating cost based on Leaving Load Temperature (LLT)
% and EER from Heat Pump manual (Based on EST 50 deg G
% (http://residential.geocomfort.com/products/literature/20D082-08NN)
%
EER = [20.0, 21.5, 23.0];
LLT = [33.6, 38.1, 42.6];
llt2HeerFit = polyfit(LLT,EER, 1); %BtuH/W
up = 0.68;
ud = 0.84;
coil.WellPumpKw = coil.CapacityTons.*((0.7457.*2.25.*240) ./ (3960*up*ud));
coil.HeatPumpEer = polyval(llt2HeerFit, coil.EntFluidTemp);
coil.HeatPumpOnlyKw = coil.TotalCap./(coil.HeatPumpEer*1000);
coil.HeatPumpKw = coil.HeatPumpOnlyKw + coil.WellPumpKw;
coil.HeatPumpKwPerTon = coil.HeatPumpKw./coil.CapacityTons;


% calculate Chiller operating cost based on Leaving Load Temperature (LLT)
% and EER from Trane software TOPSS
% 85 outside air temp 60 Ton Air chiller, See email to Paul
%
EER = [10.6, 10.9, 11.25, 11.6, 11.9];
LLT = [34,   36,   38,    40,   42];
llt2CeerFit = polyfit(LLT,EER, 1); %BtuH/W
coil.ChillerEer = polyval(llt2CeerFit, coil.EntFluidTemp);
coil.ChillerKw = coil.TotalCap./(coil.ChillerEer*1000);
coil.ChillerKwPerTon = coil.ChillerKw./coil.CapacityTons;

% coil.TotalKw = coil.FanKw + coil.PumpKw + coil.HeatPumpKw;
coil.TotalKw = coil.FanKw + coil.PumpKw + coil.ChillerKw;
coil.TotalKwPerTon = coil.TotalKw./coil.CapacityTons;
coil.demandCost = coil.TotalKw.*14.27;
coil.demandCostPerTon = coil.TotalKwPerTon.*14.27;