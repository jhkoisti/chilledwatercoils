%%
% Use loadData before using this script

% close all;
% figure
% set(gcf, 'Position', get(0,'Screensize')); % Maximize figure.

rows = coil.TotalCap >= (33*6000) & coil.SHR <= 0.67 & coil.FaceVelocity < 200   ;
dat = coil(rows,coil.Properties.VariableNames);

% SHR
% kw/ton
% CFM
% GPM
% Temp

byKw = sortrows(dat,'TotalKwPerTon');
bySHR = sortrows(dat,'SHR');

ax = [];
ax(end+1) = subplot(321);
plot(byKw.TotalKwPerTon, byKw.SHR, 'x')
ylabel('SHR')
grid minor
ylim([min(coil.SHR)*0.99 0.7])

ax(end+1) = subplot(322);
plot(byKw.TotalKwPerTon, byKw.CFM, 'x')
hold on
plot(byKw.TotalKwPerTon, byKw.CapitalCost, 'o')
hold off
legend({'CFM', 'Capital Cost'}, ...
    'Location', [0.9056    0.8902    0.0557    0.0324]);
ylabel('CFM, Capital Cost')
grid minor
% ylim([0 8500])
ylim([min(coil.CapitalCost)*0.9 max(coil.CFM)*1.1])
ax(end+1) = subplot(323);
plot(byKw.TotalKwPerTon, byKw.GPM, 's')
hold on
plot(byKw.TotalKwPerTon, byKw.EntFluidTemp, 'x')
plot(byKw.TotalKwPerTon, byKw.Height, 'o')
plot(byKw.TotalKwPerTon, byKw.CapacityTons, '*')
hold off

legend({'GPM', 'Temp', 'Height', 'Tons'}, ...
    'Location', [0.4655    0.5622    0.0396    0.0616]);
ylabel('GPM, Temp, Height, Tons');
grid minor
% ylim([20 50])
ylim([min(coil.CapacityTons)*0.9 max(coil.GPM)*1.1])


ax(end+1) = subplot(324);
plot(byKw.TotalKwPerTon, byKw.Rows, 'x')
hold on
plot(byKw.TotalKwPerTon, byKw.FinTypeNumber, 'o')
plot(byKw.TotalKwPerTon, byKw.CircuitType, '*')
hold off
ylabel('Rows, Fin Type, Circuits')
grid minor
% ylim([4 12])
ylim([0 max(coil.Rows)*1.1])

ax(end+1) = subplot(325);
plot(byKw.TotalKwPerTon, byKw.FaceVelocity, 'x')
ylabel('Face Velocity')
grid minor
% ylim([4 12])
ylim([min(coil.FaceVelocity)*0.9 max(coil.FaceVelocity)*1.1])
xlabel('kW/Ton')

ax(end+1) = subplot(326);
plot(byKw.TotalKwPerTon, byKw.demandCostPerTon, 'x')
ylabel('Demand Cost/Ton')
grid minor
% ylim([4 12])
ylim([min(coil.demandCostPerTon)*0.9 max(coil.demandCostPerTon)*1.1])

xlabel('kW/Ton')
xlim([min(coil.TotalKwPerTon)*0.9, max(coil.TotalKwPerTon)*1.1])
linkaxes(ax, 'x')
