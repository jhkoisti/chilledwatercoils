loadData

rows = coil.TotalCap >= (45*6000) & coil.SHR <= 0.70;
dat = coil(rows,coil.Properties.VariableNames);

scatter(dat.SHR, dat.TotalKwPerTon, 36+((dat.CapitalCost-1000)./15));
title('SHR vs Costs');
ylabel('Demand Cost ($)');
xlabel('SHR');
legend({'Capital Cost (bubble Size)'}, 'Location', 'NorthWest')
figure
plot(dat.SHR, dat.FaceVelocity, 'rs');
title('SHR vs Face Velocity');
ylabel('Face Velocity (fpm)');
xlabel('SHR');

figure
scatter(dat.SHR, dat.TotalKwPerTon, 10+2.*((dat.FaceVelocity-min(dat.FaceVelocity))));
title('SHR vs Costs');
ylabel('Demand Cost ($)');
xlabel('SHR');
legend({'Face Velocity (bubble Size)'}, 'Location', 'NorthWest')